package ca.polymtl.inf8405.view.activities;

import java.text.MessageFormat;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import ca.polymtl.inf8405.R;
import ca.polymtl.inf8405.model.SoundPlayer;
import ca.polymtl.inf8405.model.DataProvider;
import ca.polymtl.inf8405.model.GameAttempt;
import ca.polymtl.inf8405.model.GameCategory;
import ca.polymtl.inf8405.model.GameLevel;
import ca.polymtl.inf8405.view.game.GameView;
import ca.polymtl.inf8405.view.utils.UIHelper;
import ca.polymtl.inf8405.view.utils.VictoryDialog;

public class PlayActivity extends Activity {

	public static final String EXTRA_GRID_SIZE = "grid_size";
	public static final String EXTRA_GAME_LEVEL = "game_level";
	
	private GameAttempt gameAttempt;
	private GameView gameView;
	private SoundPlayer soundPlayer;
	private MenuItem completionRateLabel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		gameView = new GameView(this);
		soundPlayer = SoundPlayer.create(this);
		
		final int gridSize = getIntent().getExtras().getInt(EXTRA_GRID_SIZE);
		final int level =  getIntent().getExtras().getInt(EXTRA_GAME_LEVEL);
		
		GameCategory gameCategory = DataProvider.getInstance(this).getData().getGameCategory(gridSize); 
		GameLevel gameLevel = gameCategory.getLevel(level);
		gameAttempt = GameAttempt.startNewAttempt(gameCategory, gameLevel);		
		gameView.load(gameAttempt.getGrid());		
		setContentView(gameView);

		initActionBar();
	}
	
	private void initActionBar() {
		final ActionBar actionBar = getActionBar();

		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		actionBar.setTitle(gameAttempt.getGameLevel().toString());
		actionBar.setSubtitle(gameAttempt.getGameCategory().toString());
	}
	
	
	@Override
	protected void onPause() {
		soundPlayer.stop();
		super.onPause();
	}

	
	@Override
	protected void onDestroy() {
		soundPlayer.dispose();
		super.onDestroy();
	}
	
	
	public SoundPlayer getSoundPlayer() {
		return soundPlayer;
	}

	
	public void refreshGameProgressInfo() {
		
		int numerator = gameAttempt.getGrid().getTubesReachedTargetTokenCount();
		int denominator = gameAttempt.getGrid().getPairOfTokensCount();
		double fraction = Double.valueOf(numerator)/Double.valueOf(denominator);
		completionRateLabel.setTitle(MessageFormat.format("Flow : {0}/{1} ({2,number,#.##%})", numerator, denominator, fraction));
	}

	
	public void onLevelCompleted() {
		
		int currentLevel = gameAttempt.getGameLevel().getLevel();
		gameAttempt.getGameCategory().completeLevel(currentLevel);
		DataProvider.getInstance(this).saveData();
		
		DialogFragment dialog = VictoryDialog.newInstance(this);
		dialog.show(getFragmentManager(), "victory");
	}

	public void restartLevel() {
		gameAttempt.restart();
		gameView = new GameView(this);
		gameView.load(gameAttempt.getGrid());
		setContentView(gameView);
		refreshGameProgressInfo();
	}
	
	/** 
	 * Appel�e lorsque l'orientation de la tablette change.
	 * 
	 * @see android.app.Activity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		
		// bon c'est pas ce qui a de plus �l�gant mais pourquoi se compliquer la vie en tentant d'impl�menter Parcelable.
		DataProvider.getInstance(this).setSavedAttempt(gameAttempt);
		super.onSaveInstanceState(outState);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		
		gameAttempt = DataProvider.getInstance(this).getSavedAttempt();
		gameView.load(gameAttempt.getGrid());		
		setContentView(gameView);
		
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		
		getMenuInflater().inflate(R.menu.activity_play, menu);
		completionRateLabel = menu.findItem(R.id.label_completion_rate);
		refreshGameProgressInfo(); // initalise le score de la partie
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		case ca.polymtl.inf8405.R.id.menu_reset_grid:
			restartLevel();
			return true;
			
		case ca.polymtl.inf8405.R.id.menu_quit:
			// are you sure...
			UIHelper.confirmAndThenShutDown(this);
			return true;
			
		case android.R.id.home:
			goBack();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void goBack() {
		finish();
	}
	
	public void goToNextLevelOrGoBackIfAny() {
		
		int nextLevel;
		int nextGridSize;
		
		if(gameAttempt.getGameLevel().getLevel() == gameAttempt.getGameCategory().getLevelCount()) {
			nextGridSize = gameAttempt.getGameCategory().getGridSize() + 1;
			GameCategory nextGameCategory = DataProvider.getInstance(this).getData().getGameCategory(nextGridSize);
			if(nextGameCategory == null) {
				goBack();
				return;
			} else {
				nextLevel = 1;
			}
		} else {
			nextGridSize = gameAttempt.getGameCategory().getGridSize();
			nextLevel = gameAttempt.getGameLevel().getLevel() + 1;
		}
			
		finish();
		Intent nextLevelIntent = new Intent(this, PlayActivity.class);
		nextLevelIntent.putExtra(PlayActivity.EXTRA_GAME_LEVEL, nextLevel);
		nextLevelIntent.putExtra(PlayActivity.EXTRA_GRID_SIZE, nextGridSize);
		startActivity(nextLevelIntent);
	}	
}
