package ca.polymtl.inf8405.view.game;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import android.graphics.Canvas;
import android.graphics.Point;
import ca.polymtl.inf8405.model.Cell;

public class Tube extends FlowShape {

	private Cell currentCellPosition;
	private Cell lastCellPosition;
	
	private Token firstToken;
	private Token lastToken;
	
	private Set<Cell> filed;
	private Stack<TubeSection> sections;
	
	public Tube (Token startToken, Token targetToken, int cellSize) {
		
		super(startToken.getColor(), cellSize);
		
		this.filed 		= new HashSet<Cell>(); 
		this.sections 	= new Stack<TubeSection>();
		
		assert(targetToken != null);
		
		this.firstToken = startToken;
		this.lastToken = targetToken;
		
		this.currentCellPosition = new Cell(startToken.getPosition());
		this.lastCellPosition = new Cell(startToken.getPosition());
		
		filed.add(currentCellPosition);
	}
	

	/**
	 * Methode qui lie le tube avec un token.
	 * 
	 * @param lastToken Le dernier token qui sera reli� au tube.
	 */
	public void joinToToken(Token lastToken) {
		this.lastToken = lastToken;
	}
	
	/**
	 * @param x position axe des X en pixel
	 * @param y position axe des Y en pixel
	 */
	public void refresh(int x, int y) {
		currentCellPosition = new Cell(x/getCellSize(), y/getCellSize());
		
		if(stillInSameCell()) {
			resize(new Point(x,y));
		} else if(isLegalMove()) {
			if(isMovingFoward()) {
				addNewSection();
			} else {
				eraseDownToCurrent();
			}
			
		} 
	}
	
	private void resize(Point figerPosition) {
		if(!sections.isEmpty()) {
			sections.peek().resize(figerPosition);
		}
	}
	
	public void draw(Canvas canvas) {
		for(TubeSection section : sections) {
			section.draw(canvas);
		}
	}

	private void eraseDownToCurrent() {
		splitDownTo(currentCellPosition);
	}


	private void addNewSection() {
		freezeLastSection();
		sections.add(new TubeSection(lastCellPosition,currentCellPosition, getCellSize(), getColor()));
		lastCellPosition = currentCellPosition;
	}
	
	private boolean stillInSameCell() {
		return currentCellPosition.equals(lastCellPosition);
	}

	private boolean isMovingFoward() {
		boolean value = filed.add(currentCellPosition);
		return value;
	}

	private boolean isLegalMove() { 
		int dx = Math.abs(lastCellPosition.x - currentCellPosition.x);
		int dy = Math.abs(lastCellPosition.y - currentCellPosition.y);
		
		// Do not allow moving more then one cell at the time
		if (dx > 1 || dy > 1) return false;
		// Do not allow diagonals 
		if(dx == dy) return false;
		return true;
	}
	
	/**
	 * @see ca.polymtl.inf8405.model.FlowShape#isCrossing(ca.polymtl.inf8405.model.Cell)
	 */
	public boolean isCrossing(Cell cell) {
		for(TubeSection section : sections) {
			if(section.isCrossing(cell))
				return true;
		}
		return false;
	}	
	
	/**
	 * Methode qui verifie si le tube a atteint son token final.
	 * 
	 * @return True: Le tube a atteint son token. <br/>False: Autrement.
	 */
	public boolean hasReachedTargetToken() {
		return isCrossing(lastToken.getPosition());
	}

	@Override
	public void setCellSize(int size) {
		super.setCellSize(size);
		for(TubeSection section : this.sections) {
			section.setCellSize(size);
		}
	}
	
	/**
	 * Methode qui verifie que le tube a traverse au moins une case dans la surface.
	 * 
	 * @return Vrai: le tube a traverse au moins une surface.<br/>Faux: Sinon.
	 */
	public boolean notMovedFromInitialToken() {
		if (filed.isEmpty())
			return true;
		
		return false;
	}
	
	public void freezeLastSection() {
		if(!sections.empty()) {
			sections.peek().freeze();
		}
	}

	public void splitDownTo(Cell crossingCell) {
		TubeSection section;
		
		while(!sections.empty()) {
			currentCellPosition = lastCellPosition;
			section = sections.pop();
			lastCellPosition = section.getCell();
			filed.remove(lastCellPosition);
			if (section.isCrossing(crossingCell)) 
				if (section.getCell() != this.firstToken.getPosition()) break;
		}
		currentCellPosition = lastCellPosition;
		lastCellPosition = sections.empty() ? this.firstToken.getPosition() : sections.peek().getCell();
	}


	public boolean isNextTo(Cell currentCell) {
		Cell tmp = this.currentCellPosition;
		this.currentCellPosition = currentCell;
		boolean isNextTo = isLegalMove();
		this.currentCellPosition = tmp;
		return isNextTo;
	}
}
