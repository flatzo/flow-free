package ca.polymtl.inf8405.view.utils;

import ca.polymtl.inf8405.R;
import ca.polymtl.inf8405.view.activities.ShutdownActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

public class UIHelper {

	public static void confirmAndThenShutDown(final Activity activity) {
		new AlertDialog.Builder(activity, AlertDialog.THEME_DEVICE_DEFAULT_DARK)
		.setMessage(R.string.shutdown_confirmation_question)
		.setNegativeButton(android.R.string.yes, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				shutDownApplication(activity);
			}
		})
		.setPositiveButton(android.R.string.no, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		})
		.setTitle(R.string.menu_quit)
		.setIcon(R.drawable.glyphicons_063_power)
		.create()
		.show();
	}
	
	private static void shutDownApplication(final Activity activity) {
		Intent intent = new Intent(activity, ShutdownActivity.class);
	    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    activity.startActivity(intent);
	}
	
}
