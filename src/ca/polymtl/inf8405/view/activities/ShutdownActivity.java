package ca.polymtl.inf8405.view.activities;

import android.app.Activity;
import android.os.Bundle;

/**
 * Tweek permettant de fermer l'application. Le seul moyen est d'ouvrir une
 * application qui vide la pile des autres activit�s en attente et qui s'auto
 * �limine ensuite.
 * 
 */
public class ShutdownActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// release the application data.
		Runtime.getRuntime().runFinalization();
		
		// self-destroy
		finish();
	}
}
