package ca.polymtl.inf8405.view.activities;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;
import ca.polymtl.inf8405.R;
import ca.polymtl.inf8405.model.DataProvider;
import ca.polymtl.inf8405.model.GameCategory;
import ca.polymtl.inf8405.model.GameLevel;

public class LevelFragment extends Fragment implements View.OnClickListener {

	private View layout;
	private GameCategory gameCategory;
	
	public static final String ARG_CATEGORY_NUMBER = "category_number";
	
	 @Override
	public void onStart() {
	    refreshLevelButtonState();
		super.onStart();
	}	


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        layout = inflater.inflate(ca.polymtl.inf8405.R.layout.fragment_level, null);	
        
        gameCategory = DataProvider.getInstance(getActivity()).getData().getGameCategory(getArguments().getInt(ARG_CATEGORY_NUMBER));
        initButtons();
        refreshLevelButtonState();

        return layout;
	 }

	public void initButtons() {
		
		ToggleButton buttonLevel1 = (ToggleButton) layout.findViewById(R.id.buttonLevel1);
		buttonLevel1.setOnClickListener(this);
		GameLevel level1 = gameCategory.getLevel(1);
		buttonLevel1.setTag(level1);
		
		ToggleButton buttonLevel2 = (ToggleButton) layout.findViewById(R.id.buttonLevel2);
		buttonLevel2.setOnClickListener(this);
		GameLevel level2 = gameCategory.getLevel(2);
		buttonLevel2.setTag(level2);
		
		ToggleButton buttonLevel3 = (ToggleButton) layout.findViewById(R.id.buttonLevel3);
		buttonLevel3.setOnClickListener(this);
		GameLevel level3 = gameCategory.getLevel(3);
		buttonLevel3.setTag(level3);
	}
		
	public void refreshLevelButtonState() {
		
		ToggleButton buttonLevel1 = (ToggleButton) layout.findViewById(R.id.buttonLevel1);
		ToggleButton buttonLevel2 = (ToggleButton) layout.findViewById(R.id.buttonLevel2);
		ToggleButton buttonLevel3 = (ToggleButton) layout.findViewById(R.id.buttonLevel3);
		
		GameLevel level1 = (GameLevel) buttonLevel1.getTag();
		buttonLevel1.setEnabled( level1.isUnlocked() );
		buttonLevel1.setChecked( level1.isCompleted() );
		
		GameLevel level2 = (GameLevel) buttonLevel2.getTag();
		buttonLevel2.setEnabled( level2.isUnlocked() );
		buttonLevel2.setChecked( level2.isCompleted() );
		
		GameLevel level3 = (GameLevel) buttonLevel3.getTag();
		buttonLevel3.setEnabled( level3.isUnlocked() );
		buttonLevel3.setChecked( level3.isCompleted() );
	}
	
	public void goTo(GameLevel level) {
		
		Intent playLevel = new Intent(getActivity(), PlayActivity.class);
		playLevel.putExtra(PlayActivity.EXTRA_GAME_LEVEL, level.getLevel());
		playLevel.putExtra(PlayActivity.EXTRA_GRID_SIZE, gameCategory.getGridSize());
		startActivity(playLevel);
	}

	@Override
	public void onClick(View v) {
		ToggleButton button = (ToggleButton) v;
		button.setChecked(!button.isChecked()); // sinon, le bouton change d'etat alors que le niveau n'est toujours pas complete
		GameLevel level = (GameLevel) button.getTag();
		goTo( level );
	}
	
}
