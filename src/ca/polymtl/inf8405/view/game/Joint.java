package ca.polymtl.inf8405.view.game;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import ca.polymtl.inf8405.model.Cell;
import ca.polymtl.inf8405.model.Color;

public class Joint extends FlowShape {
	
	private ShapeDrawable drawable;
	private Cell position;
	
	public Joint(Cell position, int cellSize, Color color) {
		super(color, cellSize);
		
		this.cellSize = cellSize;
		this.position = position;
		
		drawable = new ShapeDrawable(new OvalShape());
		drawable.getPaint().setColor(color.getRGB());
		
		setPosition(position.convertCellToPoint(cellSize));
	}

	
	/**
	 * @see ca.polymtl.inf8405.model.FlowShape#draw(android.graphics.Canvas)
	 */
	public void draw(Canvas canvas) {
		this.drawable.draw(canvas);
	}

	public void setPosition(Point targetPoint) {
		int left	= targetPoint.x  - cellSize / 2 + super.getPadding();
		int right	= targetPoint.x  + cellSize / 2 - super.getPadding();
		
		int top		= targetPoint.y - cellSize / 2 + super.getPadding();
		int bottom	= targetPoint.y + cellSize / 2 - super.getPadding();
		
		Rect bounds = new Rect(left, top, right, bottom);
		drawable.setBounds(bounds);
	}
	
	public Cell getPosition() {
		return position;
	}
	
	/**
	 * @see ca.polymtl.inf8405.model.FlowShape#isCrossing(ca.polymtl.inf8405.model.Cell)
	 */
	public boolean isCrossing(Cell cell) {
		return cell.equals(position);
	}


	@Override
	public void setCellSize(int size) {
		super.setCellSize(size);
		setPosition(position.convertCellToPoint(size));
	}
}
