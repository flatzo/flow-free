package ca.polymtl.inf8405.model;

/**
 * Repr�sente une partie.
 */
public class GameAttempt {

	private final GameLevel gameLevel;
	private final GameCategory gameCategory;
	private long startTimeMillis;
	private PlayGrid grid;

	/**
	 * Une partie doit �tre intialis� � partir d'un niveau et sa taile de grille
	 * (GameCategory) correspondante.
	 * 
	 * @param category
	 *            Cat�gorie de grille.
	 * @param level
	 *            Niveau � jouer, son �tat doit �tre "D�v�rouill�".
	 * @return La partie g�n�r� en utilisant les informations pass�s en
	 *         param�tre.
	 */
	public static GameAttempt startNewAttempt(GameCategory category,
			GameLevel level) {
		assert (level.isUnlocked() == true);
		return new GameAttempt(category, level);
	}

	private GameAttempt(GameCategory gameCategory, GameLevel gameLevel) {
		this.gameCategory = gameCategory;
		this.gameLevel = gameLevel;
		initAttempt();
	}

	/**
	 * Lorsqu'une nouvelle partie est cr�er, on copie l'�tat initial de la
	 * grille qui concorde avec le niveau choisi. On initialise aussi une variable
	 * qui permet de connaitre le temps de jeu en tout temps.
	 */
	public void initAttempt() {
		this.grid = gameLevel.getNewGridInstance();
		this.startTimeMillis = System.currentTimeMillis();
	}

	/**
	 * R�initialise la partie.
	 * 
	 * @see initAttempt()
	 */
	public void restart() {
		initAttempt();
	}

	public long getStartTime() {
		return startTimeMillis;
	}

	public long getTimeElapsed() {
		return System.currentTimeMillis() - startTimeMillis;
	}

	public GameLevel getGameLevel() {
		return gameLevel;
	}

	public GameCategory getGameCategory() {
		return gameCategory;
	}

	public PlayGrid getGrid() {
		return grid;
	}

}
