package ca.polymtl.inf8405.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Repr�sente un niveau. Un niveau poss�de �tat (V�rouill�, D�v�rouill� et
 * R�ussi) et une grille de d�part � partir de laquelle les parties seront
 * initialis�.
 */
public class GameLevel implements Serializable {

	private static final long serialVersionUID = -8142117532380252895L;

	public final static int LEVEL_STATE_COMPLETED = 2;
	public final static int LEVEL_STATE_UNLOCKED = 1;
	public final static int LEVEL_STATE_LOCKED = 0;

	private final int level;
	private int state;
	private transient PlayGrid grid = PlayGrid.EMPTY;

	public GameLevel(final int level) {
		this.level = level;
		this.state = LEVEL_STATE_LOCKED;
	}

	public GameLevel(final int level, final int state) {
		this.level = level;
		this.state = state;
	}

	public int getLevel() {
		return level;
	}

	public int getState() {
		return state;
	}

	public void setState(final int state) {
		this.state = state;
	}

	public boolean isUnlocked() {
		return this.state != LEVEL_STATE_LOCKED;
	}

	public boolean isCompleted() {
		return this.state == LEVEL_STATE_COMPLETED;
	}

	/**
	 * @return Une nouvelle grille cr��e � partir de la grille de r�f�rence pour
	 *         ce niveau.
	 */
	public PlayGrid getNewGridInstance() {
		return new PlayGrid(grid);
	}

	public void setGrid(PlayGrid grid) {
		this.grid = grid;
	}

	public static ArrayList<GameLevel> generateLevelsAndUnlockTheFirstOne(
			int numberOfLevel) {
		ArrayList<GameLevel> levels = new ArrayList<GameLevel>(numberOfLevel);
		for (int i = 1; i <= numberOfLevel; ++i) {
			levels.add(new GameLevel(i, LEVEL_STATE_LOCKED));
		}

		if (numberOfLevel > 0)
			levels.get(0).setState(LEVEL_STATE_UNLOCKED);

		return levels;
	}

	@Override
	public String toString() {
		return "Level " + level;
	}
}
