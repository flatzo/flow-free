package ca.polymtl.inf8405.model;

import java.util.ArrayList;

import android.util.Log;
import ca.polymtl.inf8405.view.game.Token;
import ca.polymtl.inf8405.view.game.Tube;

public class PlayGrid {
	
	public final static PlayGrid EMPTY = new PlayGrid(0);
		
	private final int size;
	private final ArrayList<Token> tokens;
	private ArrayList<Tube> tubes;
	 
	public PlayGrid(final int size) {
		super();
		this.size = size;
		this.tokens = new ArrayList<Token>();
		this.tubes = new ArrayList<Tube>();
	}
	
	public PlayGrid(final PlayGrid src) {
		this.size = src.getSize();
		this.tokens = new ArrayList<Token>(src.tokens);
		this.tubes = new ArrayList<Tube>(src.tubes);
	}
	
	public int getSize() {
		return this.size;
	}
	
	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public ArrayList<Tube> getTubes() {
		return tubes;
	}
	
	public boolean existsTubeCrossing(Cell cell) {
		
		for(Tube tube : tubes) {
			if(tube.isCrossing(cell)) {
				return true;
			}
		}
		
		return false;
	}
	

	public boolean existsTubeCrossing(Cell cell, Color exceptColor) {
		
		if(exceptColor == null)
			return existsTubeCrossing(cell);
		
		for(Tube tube : tubes) {
			if(exceptColor != tube.getColor() && tube.isCrossing(cell)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @return Le premier tube qui croise une certaine cellule et qui n'est pas de la couleur sp�cifi�.
	 */
	public Tube getTubeCrossing(Cell cell, Color exceptColor) {
		
		for(Tube tube : tubes) {
			if(exceptColor != tube.getColor() && tube.isCrossing(cell)) {
				return tube;
			}
		}
		
		return null;
	}
	
	/**
	 * Methode qui verifie que la cellule fait partie des limites du grid.
	 * 
	 * @param cell Cellule a verifier.
	 * 
	 * @return Vrai: La cellule se trouve dans le grid.<br/>Faux: Elle ne se trouve pas.
	 */
	public boolean isCellInGrid(Cell cell) {
		return cell.x < size && cell.y < size;
	}
	
	/**
	 * Methode qui va rechercher dans le PlayGrid le token complementaire de celui passe en parametre.
	 * 
	 * @param myToken Le token a chercher
	 * @return Le token complementaire
	 */
	public Token findComplentaryToken(Token myToken) {
		for(Token token : tokens) {
			if(token != myToken && token.getColor() == myToken.getColor()) {
				return token;
			}
		}
		
		assert(false); //Problem! should never happen.
		return null; 
	}
	

	/**
	 * @return Le premier tube de la couleur sp�cifi�.
	 */
	public Tube findTubeByColor(Color tubeColor) {
		for(Tube tube : tubes) {
			if(tube.getColor() == tubeColor) {
				return tube;
			}
		}
		return null;
	}
	
	/**
	 * Methode qui va supprimer un tube d'une couleur particuliere.
	 * 
	 * @param color La couleur a chercher.
	 * 
	 * @return True: Le tube de cette couleur a ete trouvee et supprimee. <br/>False: Autrement.
	 */
	public boolean deleteTubeOfColor(Color color) {
		boolean foundTube = false;
		
		ArrayList<Tube> newList = new ArrayList<Tube>(tubes);
		for(Tube tube : tubes) {
			if(tube.getColor() == color) {
				
				newList.remove(tube);
				foundTube = true;
			}
		}
		tubes = newList;
		return foundTube;
	}
	
	public boolean existsTokenAt(Cell cell, Color execeptColor) {
		for(Token token : tokens) {
			if(token.getColor() != execeptColor && token.isCrossing(cell)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Methode qui verifie si tous les tokens sont relise à un tube
	 * 
	 * @return Vrai: Tous les tokens sont relies à un tube.<br/>False: Sinon.
	 */
	public boolean areAllTokensLinked() {

		for (Token token : tokens) {
			if(!existsTubeCrossing(token.getPosition())) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Methode qui va verifier que chaque tube du jeu est relie avec ses deux tokens.
	 * 
	 * @return Vrai: Chaque tube est relie par ses deux tokens.<br/>Faux: Sinon.
	 */
	public boolean areAllTubesReachedTargetToken() {
		Log.i("Tube", "Size of Tube :" + tubes.size());
		for(Tube tube : tubes) {
			if(!tube.hasReachedTargetToken()) {
				Log.i("Tube", "Tube not reached token of color: " + tube.getColor());
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @return Le nombre de paire de token qui sont reliee.
	 */
	public int getTubesReachedTargetTokenCount() {
		int count = 0;
		for(Tube tube : tubes) {
			if(tube.hasReachedTargetToken()) {
				count++;
			}
		}
		return count;
	}
	
	public int getPairOfTokensCount() {
		
		return tokens.size()/2;
	}
	
	/**
	 * Methode qui va verifier que toutes les cellules du jeu sont remplies par des tubes.
	 * 
	 * @return Vrai: Chaque cellule est rempli par un tube.<br/>Faux: Sinon.
	 */
	public boolean areAllCellsFilled() {
		
		for(int i = 0; i < size; ++i) {
			for(int j = 0; j < size; ++j) {
		
				Cell cell = new Cell(i, j);
				boolean existTubeCrossing = existsTubeCrossing(cell);
				for(Token token : tokens) {
					if(!token.isCrossing(cell) && !existTubeCrossing) {
						Log.i("Cell not completed", "Cell = (" + cell.x + ", " + cell.y + ")");
						return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * @return Si le tableau est complete, i.e. si tous les tokens sont relies et si toutes les cases sont occupees.
	 */
	public boolean isFullyCompleted() {
		
		if(areAllTokensLinked() == true) {
			if(areAllCellsFilled() == true) {
				Log.i("is grid completed", Boolean.TRUE.toString()); 
				return true;
			}
		}
		
		return false;
	}


	
}
