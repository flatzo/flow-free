package ca.polymtl.inf8405.view.game;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import ca.polymtl.inf8405.model.Cell;
import ca.polymtl.inf8405.model.Color;

public class Token extends FlowShape {

	private ShapeDrawable drawable;
	private Cell position;
	private int padding;

	public Token(Cell cell, Color color) {
		super(color, 0);
		position = cell;
		padding = 5;
		this.drawable = new ShapeDrawable(new OvalShape());
		this.drawable.getPaint().setColor(color.getRGB());
	}

	public Cell getPosition() {
		return position;
	}

	public void setPosition(Cell position) {
		this.position = position;
	}

	/**
	 * @see ca.polymtl.inf8405.model.FlowShape#draw(android.graphics.Canvas)
	 */
	public void draw(Canvas canvas) {
		this.drawable.draw(canvas);
	}

	/**
	 * Methode qui verifie si les points(en pixel) passe en parametre sont a l'interieur du Token
	 * 
	 * @param x L'axe des X en pixel
	 * @param y L'axe des X en pixel
	 * @return True: Le point est � l'int�rieur du Token. <br/>
	 * Faux: Sinon
	 */
	public boolean covers(int x, int y) {
		Rect rect = drawable.getBounds();
		return rect.contains(x, y);
	}
	

	/**
	 * @see ca.polymtl.inf8405.model.FlowShape#isCrossing(ca.polymtl.inf8405.model.Cell)
	 */
	public boolean isCrossing(Cell cell) {
		return position.x == cell.x && position.y == cell.y;
	}
	
	public void setBounds() {
		int x = position.x * getCellSize() + padding / 2;
		int y = position.y * getCellSize() + padding / 2;
		drawable.setBounds(x, y, x + getCellSize() - padding, y + getCellSize() - padding );
	}

	@Override
	public void setCellSize(int size) {
		super.setCellSize(size);
		setBounds();
		padding = (int)(0.1 * getCellSize());
	}

}
