package ca.polymtl.inf8405.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import ca.polymtl.inf8405.R;
import ca.polymtl.inf8405.model.DataProvider;
import ca.polymtl.inf8405.view.utils.UIHelper;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// initialisation du singleton
		DataProvider.getInstance(this);
		
		Button button = (Button) findViewById(R.id.startButton);
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				goToLevelSelectionActivity();
			}
		});		
	}
	
	public void goToLevelSelectionActivity() {
		Intent selectGridSize = new Intent(this, SelectLevelActivityEnhanced.class);
		startActivity(selectGridSize);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		
		case ca.polymtl.inf8405.R.id.menu_quit:
			// are you sure...
			UIHelper.confirmAndThenShutDown(this);
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

}
