package ca.polymtl.inf8405.model;

import android.content.Context;
import android.media.MediaPlayer;

public class SoundPlayer {

	private final MediaPlayer buzzMediaPlayer;
	private final MediaPlayer tooingMediaPlayer;
	private final MediaPlayer splitMediaPlayer;
	
	public static SoundPlayer create(Context context) {
		MediaPlayer buzzPlayer = MediaPlayer.create(context, ca.polymtl.inf8405.R.raw.buzzer);
		MediaPlayer tooingPlayer = MediaPlayer.create(context, ca.polymtl.inf8405.R.raw.toiing);
		MediaPlayer splitMediaPlayer = MediaPlayer.create(context, ca.polymtl.inf8405.R.raw.split);
		return new SoundPlayer(buzzPlayer, tooingPlayer, splitMediaPlayer);
	}

	private SoundPlayer(MediaPlayer buzzMediaPlayer, MediaPlayer tooingMediaPlayer, MediaPlayer splitMediaPlayer) {
		this.buzzMediaPlayer = buzzMediaPlayer;
		this.tooingMediaPlayer = tooingMediaPlayer;
		this.splitMediaPlayer = splitMediaPlayer;
	}
	
	public void buzz() {
		buzzMediaPlayer.start();
	}
	
	public void tooing() {
		tooingMediaPlayer.start();
	}
	
	public void split() {
		splitMediaPlayer.start();
	}
	
	public void stop() {
		tooingMediaPlayer.stop();
		buzzMediaPlayer.stop();
	}

	public void dispose() {
		buzzMediaPlayer.release();
		tooingMediaPlayer.release();
	}

}
