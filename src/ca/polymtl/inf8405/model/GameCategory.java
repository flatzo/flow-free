package ca.polymtl.inf8405.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Repr�sente une cat�gorie de jeu. Les cat�gories encapsule plusieurs niveaux.
 * Dans notre cas, les cat�gories sont des tailles de grilles mais nous aurions
 * aussi pu avoir des cat�rogies D�butant, Interm�diaire, Expert, ...
 * 
 */
public class GameCategory implements Serializable {

	private static final long serialVersionUID = 4335916428938032132L;

	private List<GameLevel> levels;
	private final int gridSize;

	public GameCategory(final int gridSize) {
		this.gridSize = gridSize;
		this.levels = new ArrayList<GameLevel>();
	}

	public GameLevel getLevel(final int level) {
		return levels.get(level - 1);
	}

	public List<GameLevel> getLevels() {
		return new ArrayList<GameLevel>(levels);
	}

	public void setLevels(final List<GameLevel> levels) {
		this.levels = levels;
	}

	public int getLevelCount() {
		return this.levels.size();
	}

	public int getGridSize() {
		return gridSize;
	}

	public boolean areAllLevelsCompleted() {
		for (GameLevel level : levels) {
			if (!level.isCompleted())
				return false;
		}
		return true;
	}

	/**
	 * Rend le niveau pass� en param�tre "Compl�t�" et le niveau suivant, s'il y en a un, est "D�v�rouill�".
	 * 
	 * @param level	Le niveau qui est compl�t�.
	 * @return Si un niveau a �t� d�v�rouill�.
	 */
	public boolean completeLevel(final int level) {
		getLevel(level).setState(GameLevel.LEVEL_STATE_COMPLETED);
		if (level < getLevelCount()) {
			GameLevel nextLevel = getLevel(level + 1);
			if (nextLevel.getState() == GameLevel.LEVEL_STATE_LOCKED) {
				nextLevel.setState(GameLevel.LEVEL_STATE_UNLOCKED);
				return true;
			}
		}
		
		return false;
	}

	@Override
	public String toString() {
		return gridSize + " x " + gridSize;
	}

}
