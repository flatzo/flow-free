package ca.polymtl.inf8405.view.game;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import ca.polymtl.inf8405.model.Cell;
import ca.polymtl.inf8405.model.Color;

public class TubeSection extends FlowShape {
	
	private ShapeDrawable rectangle;
	private Joint joint;
	
	private Cell startCell;
	private Cell endCell;
	
	private Point initialPoint;
	private Point targetPoint;
	public enum Direction {LEFT,TOP,RIGHT,BOTTOM};
	
	public TubeSection(Cell start, Cell end, int cellSize, Color color) {
		
		super(color, cellSize);
		
		joint = new Joint(end, cellSize, color);
		
		initialPoint = start.convertCellToPoint(cellSize);
		targetPoint = end.convertCellToPoint(cellSize);
		
		this.startCell = start;
		this.endCell = end;
		
		rectangle = new ShapeDrawable(new RectShape());
		rectangle.getPaint().setColor(color.getRGB());
		
		setRectagleScale(targetPoint);
	}
	
	private static Direction direction(Point initialPoint, Point to) {
		int dx = to.x - initialPoint.x;
		int dy = to.y - initialPoint.y;
		
		if( Math.abs(dx) < Math.abs(dy)) {
			return (dy > 0 ? Direction.BOTTOM : Direction.TOP);
		} else  {
			return (dx < 0 ? Direction.LEFT : Direction.RIGHT);
		}
	}
	
	public void draw(Canvas canvas) {
		this.joint.draw(canvas);
		this.rectangle.draw(canvas);
	}

	public void resize(Point figerPosition) {
		
		Point alignedPoint = null;
		switch (direction(initialPoint, figerPosition)) {
		case LEFT:
			//Log.d("TubeSection","LEFT");
		case RIGHT:
			alignedPoint = new Point(figerPosition.x, initialPoint.y);
			//Log.d("TubeSection","RIGHT");
			break;
		case TOP:
			//Log.d("TubeSection","TOP");
		case BOTTOM:
			alignedPoint = new Point(initialPoint.x, figerPosition.y);
			break;
		}
		joint.setPosition(alignedPoint);
		setRectagleScale(alignedPoint);
	}
	
	private void setRectagleScale(Point toPoint) {
		
		int left 	= Math.min(initialPoint.x, toPoint.x);
		int right 	= Math.max(initialPoint.x, toPoint.x);
		if(left == right) {
			left	-= getCellSize() / 2;
			right	+= getCellSize() / 2;
			left 	+= super.getPadding();
			right 	-= super.getPadding();
		} 
		
		int top		= Math.min(initialPoint.y, toPoint.y);
		int bottom	= Math.max(initialPoint.y, toPoint.y);
		if(top == bottom) {
			top 	-= getCellSize() / 2;
			bottom 	+= getCellSize() / 2;
			bottom 	-= super.getPadding();
			top		+= super.getPadding();
		} 

		Rect bounds = new Rect(left, top, right, bottom);
		rectangle.setBounds(bounds);
	}

	/**
	 * Aligne la section de tube au point milieu de la cellule.
	 */
	public void freeze() {

		setRectagleScale(targetPoint);
		joint.setPosition(targetPoint);
	}
		
	public boolean isCrossing(Cell cell) {
		
		return startCell.equals(cell) || joint.getPosition().equals(cell);
	}

	@Override
	public void setCellSize(int size) {
		super.setCellSize(size);
		this.joint.setCellSize(size);
		
		initialPoint = startCell.convertCellToPoint(this.getCellSize());
		targetPoint = endCell.convertCellToPoint(this.getCellSize());
		
		initialPoint = startCell.convertCellToPoint(getCellSize());
		targetPoint = endCell.convertCellToPoint(getCellSize());
		
		setRectagleScale(targetPoint);
		
	}
	
	public Cell getCell() {
		return this.endCell;
	}
	
}
