package ca.polymtl.inf8405.model;

import ca.polymtl.inf8405.view.game.Token;

public class PlayGridFactories {

	public static class Grid7x7 {
		
		public static PlayGrid createLevel1() {
			PlayGrid grid = new PlayGrid(7);
			grid.getTokens().add(new Token(new Cell(0,1), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(0,6), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(2, 2), Color.RED));
			grid.getTokens().add(new Token(new Cell(4, 3), Color.RED));
			grid.getTokens().add(new Token(new Cell(2, 4), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(4, 5), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(4, 4), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(1, 5), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(0, 5), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(5, 5), Color.GREEN));
			return grid;
		}
		
		public static PlayGrid createLevel2() {
			PlayGrid grid = new PlayGrid(7);
			grid.getTokens().add(new Token(new Cell(1, 1), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(2, 5), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(0, 5), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(6, 3), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(1, 5), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(6, 2), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(3, 5), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(5, 2), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(5, 4), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(5, 6), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(2, 2), Color.GREY));
			grid.getTokens().add(new Token(new Cell(5, 1), Color.GREY));
			grid.getTokens().add(new Token(new Cell(6, 4), Color.RED));
			grid.getTokens().add(new Token(new Cell(6, 6), Color.RED));
			return grid;
		}
		
		public static PlayGrid createLevel3() {
			PlayGrid grid = new PlayGrid(7);
			grid.getTokens().add(new Token(new Cell(0, 5), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(3, 4), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(1, 2), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(5, 4), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(2, 2), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(4, 2), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(1, 5), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(4, 5), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(3, 5), Color.RED));
			grid.getTokens().add(new Token(new Cell(6, 6), Color.RED));
			grid.getTokens().add(new Token(new Cell(1, 3), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(4, 4), Color.GREEN));			
			return grid;
		}
	}
	
	public static class Grid8x8 {
		
		public static PlayGrid createLevel1() {
			PlayGrid grid = new PlayGrid(8);
			grid.getTokens().add(new Token(new Cell(0, 0), Color.MAGENTA));
			grid.getTokens().add(new Token(new Cell(0, 2), Color.MAGENTA));
			grid.getTokens().add(new Token(new Cell(0, 1), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(2, 2), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(0, 3), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(6, 3), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(2, 5), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(3, 4), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(4, 0), Color.RED));
			grid.getTokens().add(new Token(new Cell(4, 5), Color.RED));
			grid.getTokens().add(new Token(new Cell(5, 1), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(7, 1), Color.BLUE));		
			grid.getTokens().add(new Token(new Cell(5, 2), Color.GREY));
			grid.getTokens().add(new Token(new Cell(6, 1), Color.GREY));
			grid.getTokens().add(new Token(new Cell(7, 2), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(7, 7), Color.WHITE));	
			grid.getTokens().add(new Token(new Cell(2, 4), Color.CYAN));
			grid.getTokens().add(new Token(new Cell(5, 3), Color.CYAN));	
			return grid;
		}
		
		public static PlayGrid createLevel2() {
			PlayGrid grid = new PlayGrid(8);
			grid.getTokens().add(new Token(new Cell(1, 6), Color.RED));
			grid.getTokens().add(new Token(new Cell(3, 4), Color.RED));
			grid.getTokens().add(new Token(new Cell(2, 6), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(5, 5), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(2, 2), Color.CYAN));
			grid.getTokens().add(new Token(new Cell(2, 4), Color.CYAN));
			grid.getTokens().add(new Token(new Cell(3, 6), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(4, 1), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(4, 0), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(6, 0), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(5, 0), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(5, 3), Color.YELLOW));		
			grid.getTokens().add(new Token(new Cell(6, 1), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(6, 3), Color.GREEN));	
			return grid;
		}
		
		public static PlayGrid createLevel3() {
			PlayGrid grid = new PlayGrid(8);
			grid.getTokens().add(new Token(new Cell(0, 3), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(3, 0), Color.GREEN));
			grid.getTokens().add(new Token(new Cell(0, 4), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(4, 1), Color.WHITE));
			grid.getTokens().add(new Token(new Cell(1, 1), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(2, 6), Color.BLUE));
			grid.getTokens().add(new Token(new Cell(2, 1), Color.MAGENTA));
			grid.getTokens().add(new Token(new Cell(3, 3), Color.MAGENTA));
			grid.getTokens().add(new Token(new Cell(2, 5), Color.RED));
			grid.getTokens().add(new Token(new Cell(4, 4), Color.RED));
			grid.getTokens().add(new Token(new Cell(3, 1), Color.ORANGE));
			grid.getTokens().add(new Token(new Cell(4, 3), Color.ORANGE));		
			grid.getTokens().add(new Token(new Cell(5, 1), Color.YELLOW));
			grid.getTokens().add(new Token(new Cell(3, 5), Color.YELLOW));	
			grid.getTokens().add(new Token(new Cell(4, 5), Color.CYAN));
			grid.getTokens().add(new Token(new Cell(5, 2), Color.CYAN));	
			return grid;
		}
		
	}
	
}
