package ca.polymtl.inf8405.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Cette classe g�re toutes les donn�es persistantes du jeu. Dans ce cas-ci les
 * seules donn�es sont les GameCategory qui eux contiennent les GameLevels dont
 * l'�tat doit �tre sauvegarder par l'application.
 */
public class GameData implements Serializable {

	private static final long serialVersionUID = 6724491813408681718L;

	// Les gameType regroupe plusieurs niveau, dans notre cas, les gameType
	// seront { 7x7 et 8x8 }.
	private TreeMap<Integer, GameCategory> gameTypes = new TreeMap<Integer, GameCategory>();

	public GameCategory getGameCategory(Integer gridSize) {
		return gameTypes.get(gridSize);
	}

	public List<GameCategory> getGameCategories() {
		return new ArrayList<GameCategory>(gameTypes.values());
	}

	public void setGameType(Integer gridSize, GameCategory gameType) {
		gameTypes.put(gridSize, gameType);
	}

	public int getCategoryCount() {
		return gameTypes.size();
	}

	/**
	 * @return Un GameData avec les niveaux 7x7 et 8x8 contenant chacun 3
	 *         niveaux.
	 */
	public static GameData createForFirstTime() {

		GameData data = new GameData();

		GameCategory category7x7 = new GameCategory(7);
		category7x7.setLevels(GameLevel.generateLevelsAndUnlockTheFirstOne(3));
		data.setGameType(7, category7x7);

		GameCategory category8x8 = new GameCategory(8);
		category8x8.setLevels(GameLevel.generateLevelsAndUnlockTheFirstOne(3));
		data.setGameType(8, category8x8);

		return data;
	}

	/**
	 * @param file
	 *            Un fichier contenant un GameData s�rialis�.
	 * @return Le GameData d�s�rialis� OU un GameData cr�er par createForFirstTime() si la d�s�rialisation �choue.
	 */
	public static GameData createFromFile(File file) {

		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object object = ois.readObject();
			return (GameData) object;

		} catch (Exception e) {
			e.printStackTrace();
			return createForFirstTime();
		}
	}

	/**
	 * @param file		Le fichier destination.
	 * @param gameData	Les donn�es � s�rialis�e.
	 * @throws IOException Si la s�rialisation �choue.
	 */
	public static void saveToFile(File file, GameData gameData)
			throws IOException {

		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(gameData);
	}

	/**
	 * Initialise les grilles de r�f�rence pour chaque niveau � partir des valeurs hardcod� par PlayGridFactories.
	 */
	public void setOrResetPlayGrid() {

		GameCategory category7x7 = gameTypes.get(7);
		assert (category7x7 != null && category7x7.getLevelCount() >= 3);
		category7x7.getLevel(1).setGrid(
				PlayGridFactories.Grid7x7.createLevel1());
		category7x7.getLevel(2).setGrid(
				PlayGridFactories.Grid7x7.createLevel2());
		category7x7.getLevel(3).setGrid(
				PlayGridFactories.Grid7x7.createLevel3());

		GameCategory category8x8 = gameTypes.get(8);
		assert (category8x8 != null && category8x8.getLevelCount() >= 3);
		category8x8.getLevel(1).setGrid(
				PlayGridFactories.Grid8x8.createLevel1());
		category8x8.getLevel(2).setGrid(
				PlayGridFactories.Grid8x8.createLevel2());
		category8x8.getLevel(3).setGrid(
				PlayGridFactories.Grid8x8.createLevel3());
	}

}
