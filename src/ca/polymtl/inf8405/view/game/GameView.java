package ca.polymtl.inf8405.view.game;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import ca.polymtl.inf8405.model.Cell;
import ca.polymtl.inf8405.model.PlayGrid;
import ca.polymtl.inf8405.view.activities.PlayActivity;

/**
 * GameView reagit aux actions de l'utilisateur en modifiant les elements de la grille.
 *
 */
@SuppressLint("ViewConstructor")
public class GameView extends View {

	private PlayGrid playGrid;
	private PlayActivity context;
	private Point screenSize, padding;
	private int cellSize;
	private Token activeToken;
	private Tube activeTube;

	private final Point DEFAULT_PADDING = new Point(5, 5);
	private BackgroundGrid grid;

	public GameView(PlayActivity context) {
		super(context);
		this.context = context;
		this.screenSize = screenSize();
		this.padding = DEFAULT_PADDING;
	}
	
	public void load(PlayGrid playGrid) {
		this.playGrid = playGrid;
		this.cellSize = (screenSize.x - padding.x) / playGrid.getSize();
		
		this.grid = new BackgroundGrid(playGrid.getSize(), cellSize, Color.BLACK, Color.GRAY);
		
		for (Token token : playGrid.getTokens()) {
			token.setCellSize(cellSize);
		}
		
		for(Tube tube : playGrid.getTubes()) {
			tube.setCellSize(cellSize);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int x = (int)event.getX();
		int y = (int)event.getY();

		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			selectTokenAt(x,y);
			if (activeToken != null) 	createTube();
			else 						selectTubeAt(x,y);
			
			break;
			
		case MotionEvent.ACTION_MOVE:
			if ( isInGridBounds(x,y) ) 	drawTube(x,y);
			break;
			
		case MotionEvent.ACTION_UP:
			
			if(playGrid.isFullyCompleted())
				context.onLevelCompleted();
			
			deselectToken();
			
			//On s'assure que le tube qui a ete cree rempli au minimum une case
			if (activeTube != null) {
				if (activeTube.notMovedFromInitialToken()) { 
					playGrid.deleteTubeOfColor(activeTube.getColor());
				}
				//Mettre a jour la vue
				activeTube.freezeLastSection();
				this.invalidate();
			}
			
			break;
		}
		return true;
	}

	private boolean isInGridBounds(int x, int y) {
		return
				x < cellSize * playGrid.getSize() &&
				y < cellSize * playGrid.getSize();
	}

	private void selectTubeAt(int x, int y) {
		Cell cell = new Cell(x / cellSize,y / cellSize);
		
		for(Tube tube: this.playGrid.getTubes()) {
			if(tube.isCrossing(cell)) {
				activeTube = tube;
				return;
			}
		}
		
	}

	/**
	 * Methode qui selectionne le token selon une position de pixel.
	 * 
	 * @param x Axe des X en pixel
	 * @param y Axe des Y en pixel
	 */
	private void selectTokenAt(int x, int y) {
		
		for(Token token : playGrid.getTokens()) {
			if(token.covers(x,y)) {
				activeToken = token;
			}
		}
	}
	
	private void deselectToken() {
		activeToken = null;
	}

	/**
	 * Methode qui cree un tube a partir d'un Token deja selectionne par la methode 'selectTokenAt(int,int)'.
	 */
	private void createTube() {
		if (playGrid.deleteTubeOfColor(activeToken.getColor()))
			context.refreshGameProgressInfo();

		activeTube = new Tube(activeToken,
				playGrid.findComplentaryToken(activeToken), cellSize);
		playGrid.getTubes().add(activeTube);
	}

	/**
	 * Met a jour le tube actif pour correspondre a la nouvelle position du doigt sur l'ecran.
	 */
	private void drawTube(int x, int y) {
		x = clipMargin(x);
		y = clipMargin(y);
		
		if (activeTube != null) {
			Cell currentCell = Cell.convertPointToCell(new Point(x, y), cellSize);
			
			if (
					// If crossing a token of any color except activeTube's color
					playGrid.existsTokenAt(currentCell, activeTube.getColor())
				) {
				// Do not draw
				context.getSoundPlayer().buzz(); // Buzz !
			} else {
				
				Tube crossingTube = playGrid.getTubeCrossing(currentCell, activeTube.getColor());
				if (
					// If crossing a tube of any color except activeTube's color
					crossingTube != null &&
					activeTube.isNextTo(currentCell)
				) {
					// Do not draw
					// Split the tube user is currently drawing through
					crossingTube.splitDownTo(currentCell);
					context.refreshGameProgressInfo();
					context.getSoundPlayer().split();
					this.invalidate();
				
				} else {
					// Draw the tube
					activeTube.refresh(x,y);
					this.invalidate();
				}
			}
				
			//J'empeche le tube de continuer a se dessiner s'il a deja atteint son token final.
			if (activeTube.hasReachedTargetToken()) {
				activeTube = null;
				context.refreshGameProgressInfo();
				context.getSoundPlayer().tooing();
			}
		}
	}

	private int clipMargin(int x) {
		int margin = cellSize / 2;
		int length = playGrid.getSize() * cellSize;
		if (x < margin) 				return margin;
		else if (x > length - margin) 	return length - margin;
		else 							return x;
	}


	private Point screenSize() {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point outSize = new Point();
		display.getSize(outSize);
		
		Log.d("GameView", "Width : " + this.getWidth() + " Height : " + this.getHeight());

		return outSize;
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		this.cellSize = (w < h ? w : h - padding.x) / playGrid.getSize();
		
		grid.setCellSize(this.cellSize);
		
		for(Tube tube : playGrid.getTubes()) {
			tube.setCellSize(this.cellSize);
		}
		for(Token token: playGrid.getTokens()) {
			token.setCellSize(this.cellSize);
		}
	}

	protected void onDraw(Canvas canvas) {
		grid.draw(canvas);

		for (Tube tube : this.playGrid.getTubes()) {
			tube.draw(canvas);
		}
		for (Token token : this.playGrid.getTokens()) {
			token.draw(canvas);
		}
	}
}