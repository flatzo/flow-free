package ca.polymtl.inf8405.model;

import java.io.Serializable;

import android.graphics.Point;

public class Cell implements Serializable {

	private static final long serialVersionUID = -2062644524948440632L;
	
	public int x;
	public int y;
	
	public Cell() {
	}

	public Cell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Cell(Cell src) {
		x = src.x;
		y = src.y;
	}
	
	public Point convertCellToPoint(int cellWidth) {
		return new Point(
				this.x * cellWidth + cellWidth / 2,
				this.y * cellWidth + cellWidth / 2);
	}

	/**
	 * Methode qui trouve la cellule correspondant a un point.
	 * 
	 * @param point Le point du jeu
	 * @param cellWidth Longueur(largeur) d'une cellule
	 * 
	 * @return Cellule du point
	 */
	public static Cell convertPointToCell(Point point, int cellWidth) {
		return new Cell(
				point.x / cellWidth,
				point.y / cellWidth);
	}
	
	@Override
	public String toString() {
		return "Cell (" + x + ", " + y + ")";
	}
	

	@Override
	public int hashCode() {
		return new Point(this.x,this.y).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

}
