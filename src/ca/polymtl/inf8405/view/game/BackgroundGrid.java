package ca.polymtl.inf8405.view.game;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;

/**
 * La grille qui apparait en fond d'ecran est une couche de ShapeDrawable
 * qui apparait derriere les tubes et les tokens.
 * 
 */
public class BackgroundGrid {

	private int size, cellSize;
	private ShapeDrawable bg;
	private ShapeDrawable[][] cells;

	public BackgroundGrid(int size, int cellSize, int bgColor, int lineColor) {
		this.size = size;
		this.cellSize = cellSize;

		bg = new ShapeDrawable(new RectShape());
		// This might look weird, background having lineColor,
		// but background here is what's behing the cells,
		// which could be considered as the background
		bg.getPaint().setColor(lineColor);

		cells = new ShapeDrawable[size][size];
		ShapeDrawable cell;
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; j++) {
				cell = new ShapeDrawable(new RectShape());
				cell.getPaint().setColor(bgColor);

				cells[i][j] = cell;
			}
		}
		setCellSize(cellSize);
	}

	public void draw(Canvas canvas) {
		bg.draw(canvas);

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				cells[i][j].draw(canvas);
			}
		}
	}

	public void setCellSize(int size) {
		this.setBgSize(size);
		this.setCellsSize(size);
	}

	private void setCellsSize(int cellSize) {
		int padding = 1;
		int left, top, right, bottom;

		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				left = i * cellSize + padding;
				top = j * cellSize + padding;
				right = (i + 1) * cellSize - padding;
				bottom = (j + 1) * cellSize - padding;
				cells[i][j].setBounds(new Rect(left, top, right, bottom));
			}
		}
	}

	private void setBgSize(int size) {
		Rect bounds = new Rect(0, 0, size * cellSize, size * cellSize);
		bg.setBounds(bounds);
	}
}
