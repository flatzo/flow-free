package ca.polymtl.inf8405.model;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.widget.Toast;
import ca.polymtl.inf8405.R;

/**
 * Ce singleton permet d'acc�der aux donn�es de l'application � partir de
 * n'importe quel contexte. Cela simplifie la gestion des donn�es entre les
 * activit�s.
 * 
 */
public class DataProvider {

	public static final String dataFilename = "data";

	private GameAttempt savedAttempt;
	private final GameData gameData;
	private final File dataFile;
	private final Context applicationContext;

	private static DataProvider instance = null;

	public static DataProvider getInstance(Context context) {

		if (instance == null) {
			final File backupFile = context.getApplicationContext()
					.getFileStreamPath(DataProvider.dataFilename);
			instance = new DataProvider(backupFile,
					context.getApplicationContext());
		}

		return instance;
	}

	private DataProvider(final File file, final Context context) {
		this.dataFile = file;
		this.applicationContext = context;

		gameData = GameData.createFromFile(file);
		gameData.setOrResetPlayGrid();
	}

	public GameData getData() {
		return gameData;
	}

	public GameAttempt getSavedAttempt() {
		return savedAttempt;
	}

	public void setSavedAttempt(GameAttempt savedAttempt) {
		this.savedAttempt = savedAttempt;
	}

	public void saveAndFreeInstance() {
		saveData();
		instance = null;
	}

	/**
	 * S�rialise toutes les donn�es de GameData dans le fichier de donn�es.
	 * Affiche un Toast informant l'utilisateur de l'�chec/r�ussite de la s�rialisation.
	 */
	public void saveData() {
		try {
			GameData.saveToFile(dataFile, gameData);
			Toast.makeText(applicationContext, R.string.data_saved_toast_text,
					Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			e.printStackTrace();
			Toast.makeText(
					applicationContext,
					String.format("fail to save data : %s ",
							e.getLocalizedMessage()), Toast.LENGTH_SHORT)
					.show();
		}
	}
}
