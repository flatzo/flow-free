package ca.polymtl.inf8405.model;


/**
 * Contient les couleurs qu'il est possible d'afficher sur la grille.
 */
public enum Color {
	
	BLUE(0xFF0000FF), 
	ORANGE(0xFFFAA100),
	RED(0xFFFE0000),
	GREEN(0xFF007A00),
	YELLOW(android.graphics.Color.YELLOW), 
	WHITE(android.graphics.Color.WHITE),
	GREY(android.graphics.Color.GRAY),
	CYAN(android.graphics.Color.CYAN),
	MAGENTA(android.graphics.Color.MAGENTA);
	
	private int rgb;
	Color(int rgb) {
		this.rgb = rgb;
	}
	public int getRGB() {
		return this.rgb;
	}
}
