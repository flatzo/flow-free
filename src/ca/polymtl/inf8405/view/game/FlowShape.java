package ca.polymtl.inf8405.view.game;

import ca.polymtl.inf8405.model.Cell;
import ca.polymtl.inf8405.model.Color;
import android.graphics.Canvas;

/**
 * Classe de base pour chaque forme pr�sente sur la grille de jeu.
 */
public abstract class FlowShape {

	private final static int PADDING_RATIO_INV = 6;
	
	protected Color color;
	protected int cellSize;
	
	public FlowShape(Color color, int cellSize) {
		this.color = color;
		this.cellSize = cellSize;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public int getCellSize() {
		return cellSize;
	}

	/**
	 * Methode qui dessine l'objet sur la scene.
	 * 
	 * @param canvas
	 */
	public abstract void draw(Canvas canvas);
	
	/**
	 * Methode qui verifie si l'objet entre en contact avec une cellule particuliere.
	 * 
	 * @param position Position de la cellule e tester.
	 * @return True: l'objet entre en contact avec la cellule.
	 * False: Sinon.
	 */
	public abstract boolean isCrossing(Cell position);
	
	/**
	 * Methode qui permet de changer la grosseur des cellules, par exemple
	 * lors du changement d'orientation
	 * 
	 * @param size
	 */
	public void setCellSize(int size) {
		this.cellSize = size;
	}
	
	
	/**
	 * @return Le padding calcule en fonction de la taille de la cellule et un ratio.
	 */
	public int getPadding() {
		return cellSize/PADDING_RATIO_INV;
	}
}
