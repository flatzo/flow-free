package ca.polymtl.inf8405.view.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import ca.polymtl.inf8405.R;
import ca.polymtl.inf8405.view.activities.PlayActivity;

/**
 * La raison pour laquelle nous n'utilisons pas tout simplement AlertDialog est
 * parce qu'on voudrait �ventuellement pouvoir cr�er une fen�tre personnalis� �
 * la place.
 * 
 */
@SuppressLint("ValidFragment")
public class VictoryDialog extends DialogFragment {

	private final MediaPlayer winningMusicPlayer;

	public static VictoryDialog newInstance(Context context) {
		VictoryDialog menu = new VictoryDialog(context);
		return menu;
	}

	private VictoryDialog(Context context) {
		this.winningMusicPlayer = MediaPlayer.create(context, R.raw.victory);
	}

	@Override
	public void onStart() {
		winningMusicPlayer.start();
		super.onStart();
	}

	@Override
	public void onStop() {
		winningMusicPlayer.stop();
		super.onStop();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity(),
				AlertDialog.THEME_DEVICE_DEFAULT_DARK)
				.setIcon(android.R.drawable.star_big_on)
				.setTitle(R.string.title_victory_dialog)
				.setPositiveButton(R.string.victory_dialog_next_level_option,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((PlayActivity) getActivity())
										.goToNextLevelOrGoBackIfAny();
							}
						})
				.setNegativeButton(R.string.victory_dialog_back_option,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((PlayActivity) getActivity()).goBack();
							}
						})
				.setNeutralButton(R.string.victory_dialog_reset_option,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((PlayActivity) getActivity()).restartLevel();
							}
						}).create();
	}

}
